# README

1. U config/application.yml postavite svoje gmail podatke.  

2. Example api request:

	localhost:3000/mailer/?datum_pocetni=20.09.2018&datum_krajnji=24.09.2018&valute=USD,EUR,AUD

	datum_pocetni --> pocetni datum od kojeg se mjeri razlika tecaja
	
	datum_krajnji --> krajnji datum do kojeg se mjeri razlika tecaja
	
	valute        --> zarezom odvojene troslovne oznake valuta koje zelimo usporediti

3. Request traje nesto manje od minute i kao rezultat, salje mail specificiran u app/mailers/tecaj_mailer

