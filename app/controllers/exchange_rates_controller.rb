class ExchangeRatesController < ApplicationController
  include ExchangeRateHelper
  before_action :set_exchange_rate, only: [:show, :update, :destroy]
  before_action :set_exchange_rate_difference, only: [:exchange_rate_difference] 
  before_action :set_user, only: [:valute]
  
  def limit
    ExchangeRateHelper.check_user_limits
  end
  
  def get_by_date
    @result = ExchangeRateHelper.get_by_date(get_by_date_params)
    if @result
      render json: @result, each_serializer: ExchangeRateSerializer, root: nil    
    else
      render json: { "error" => "Please check your input data!" }, status: :bad_request
    end
  end

  def exchange_rate_difference
    @exchange_rate_service = ExchangeRateService.new   
    @result = @exchange_rate_service.exchange_rate_differences(@exchange_rates)
    if @result
      render json: @result, status: :ok
    else 
      render json: {"message" => "Check your input data!"}, status: :bad_request
    end
  end

  # GET /exchange_rates
  def index
    @exchange_rates = ExchangeRate.all

    render json: @exchange_rates
  end

  # GET /exchange_rates/1
  def show
    render json: @exchange_rate
  end

  # POST /exchange_rates
  def create
    @exchange_rate = ExchangeRate.new(exchange_rate_params)

    if @exchange_rate.save
      render json: @exchange_rate, status: :created, location: @exchange_rate
    else
      render json: @exchange_rate.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /exchange_rates/1
  def update
    if @exchange_rate.update(exchange_rate_params)
      render json: @exchange_rate
    else
      render json: @exchange_rate.errors, status: :unprocessable_entity
    end
  end

  # DELETE /exchange_rates/1
  def destroy
    @exchange_rate.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exchange_rate
      @exchange_rate = ExchangeRate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def exchange_rate_params
      params.require(:exchange_rate).permit(:date, :buying_rate, :middle_rate, :selling_rate)
    end

    # Permit parameters for custom function get_by_date
    def get_by_date_params
      params.permit(:date, :currencies) ## nesto ne radi kako treba, a u doljnjem primjeru je ok
    end

    # Permit parameters for custom function exchange_rate_difference
    def exchange_rate_difference_params
      params.permit(:start_date, :end_date, :currencies => [])
    end

    # Set parameters for custom function exchange_rate_difference
    def set_exchange_rate_difference
      @exchange_rates = ExchangeRateHelper.set_exchange_rate(exchange_rate_difference_params)
    end



    def set_user
      @user = current_user
    end
end
