class CurrenciesController < ApplicationController
  before_action :set_currency, only: [:show, :update, :destroy]

  def hnbApi
    @hnbApiService = HnbApiService.new
    render json: @hnbApiService.update
  end 

  def first
    @hnbApiService = HnbApiService.new
    render json: @hnbApiService.first_time_update
  end 
  
  # GET /currencies
  def index
    @currencies = Currency.all

    render json: @currencies
  end

  # GET /currencies/1
  def show
    render json: @currency
  end

 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_currency
      @currency = Currency.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def currency_params
      params.require(:currency).permit(:currency, :state, :currency_code, :curency_unit)
    end
end
