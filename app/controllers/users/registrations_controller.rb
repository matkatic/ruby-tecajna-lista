# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  include ErrorsHelper
  skip_before_action :authenticate_user_from_token!
  before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /user
  def create
    user = nil
    errors = Hash.new
    if params[:name].empty?
      errors = errors.merge ErrorsHelper.name_missing
    end  
    if params[:password].empty?
      errors = errors.merge ErrorsHelper.password_missing
      if params[:password_confirmation].empty?
        errors = errors.merge ErrorsHelper.password_confirmation_missing
      end
    elsif params[:password] != params[:password_confirmation]
      errors = errors.merge ErrorsHelper.password_mismatch
    end
    if params[:email].empty?
      errors = errors.merge ErrorsHelper.email_missing
    end
    
    unless errors.any?
      if User.where(:email => params[:email]).first
        errors = errors.merge ErrorsHelper.user_exists(params[:email])
      else
        user = User.new({
          :email => params[:email],
          :name => params[:name],
          :password => params[:password],
          :password_confirmation => params[:password_confirmation]
        })
        user.save
      end
    end

    if !user.nil? && user.valid?
      sign_in :user, user
      render json: user, serializer: UserSerializer, root: nil
    else
      render json: { :errors => errors }
    end
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :email, :password])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end
