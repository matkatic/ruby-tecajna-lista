require_all 'app/serializers/user_serializer.rb'

class Users::SessionsController < Devise::SessionsController
  include ErrorsHelper
  skip_before_action :authenticate_user_from_token!

  # POST /user/login
  def create
    errors = Hash.new
    
    if params[:email].empty?
      errors = errors.merge ErrorsHelper.email_missing
    end
    
    if params[:password].empty?
      errors = errors.merge ErrorsHelper.password_missing
    end
    
    if errors.any?
      render json: {:errors => errors}
    else
      @user = User.find_for_database_authentication(email: params[:email])
      return invalid_login_attempt unless @user

      if @user.valid_password?(params[:password])
        sign_in :user, @user
      
        render json: @user, serializer: UserSerializer, root: nil
      else
        invalid_login_attempt
      end
    end
  end

  def destroy
    session[:user_id] = nil
  end

  private

  def invalid_login_attempt
    warden.custom_failure!
    render json: ErrorsHelper.invalid_login_attempt
  end

end
