class SettingsController < ApplicationController
  before_action :set_setting, only: [:show, :update, :destroy]
  before_action :set_create_params, only: [:create]
  before_action :set_user, only: [:create, :show]

  # GET /settings
  def index
    @settings = Setting.where(:users => @user)

    render json: @settings
  end

  # GET /settings/1
  def show
    @setting = Setting.where(setting_params)
    render json: @setting
  end

  # POST /settings
  def create
    @setting_service = SettingsService.new
    if @setting_service.create(set_create_params, @user)
        render json: "Setting created successfully!", status: :created, location: @setting
    else
      render json: "Setting did not save! Check your input data!", status: :unprocessable_entity
    end
  end

  # PATCH/PUT /settings/1
  def update
    if @setting.update(setting_params)
      render json: @setting
    else
      render json: @setting.errors, status: :unprocessable_entity
    end
  end

  # DELETE /settings/1
  def destroy
    @setting.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_setting
      @setting = Setting.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def set_create_params
      params.permit(:limit, :currency, :start_date)
    end

    def set_update_params
      params.permit(:limit, :start_date)
    end

    def set_show_params
      params.require(:settings).permit(:settings)
    end

    def set_user
      @user = current_user
    end
end
