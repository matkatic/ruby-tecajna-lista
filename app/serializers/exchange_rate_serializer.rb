class ExchangeRateSerializer < ActiveModel::Serializer
  attributes :id, :start_date, :buying_rate, :middle_rate, :selling_rate, :currencies
  belongs_to :currencies

  def currencies
    object.currencies
  end

end