class UserSerializer < ActiveModel::Serializer

  attributes :email, :token_type, :user_id, :access_token, :user_settings
  has_many :user_settings

  def user_id
    object.id
  end

  def settings
    object.user_settings
  end

  def token_type
    'Bearer'
  end

end