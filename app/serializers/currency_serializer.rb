class CurrencySerializer < ActiveModel::Serializer
  attributes :id, :currency, :state, :currency_code, :currency_unit
  has_many :exchange_rates 
end

