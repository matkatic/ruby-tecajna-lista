class Currency 
  include Mongoid::Document
  

  field :currency, type: String
  field :state, type: String
  field :currency_code, type: Integer
  field :currency_unit, type: Integer
  
  has_many :exchange_rates, class_name: ExchangeRate.to_s, inverse_of: :currencies
end
