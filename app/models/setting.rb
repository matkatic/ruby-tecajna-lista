class Setting

  include Mongoid::Document

  field :limit, type: Float
  field :start_date, type: DateTime
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  # RELATIONS
  belongs_to :exchange_rate, class_name: ExchangeRate.to_s, inverse_of: :settings
  belongs_to :users, class_name: User.to_s, inverse_of: :user_settings

  # SCOPES
  default_scope ->{ includes(:exchange_rate) }
  default_scope ->{ includes(:users) }

  def update_user
    byebug
    user = self.users
    user.user_settings << self
    user.save
  end

end

