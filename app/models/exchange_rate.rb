class ExchangeRate
  include Mongoid::Document
  # attr_accessor(:valuta_id, :datum, :kupovni, :srednji, :prodajni)

  field :start_date, type: DateTime
  field :buying_rate, type: Float
  field :middle_rate, type: Float
  field :selling_rate, type: Float

  # RELATIONS
  belongs_to :currencies, class_name: Currency.to_s, inverse_of: :exchange_rates
  has_many :settings, class_name: Setting.to_s, inverse_of: :exchange_rate
  # SCOPES
  default_scope ->{ includes(:currencies) }

end

