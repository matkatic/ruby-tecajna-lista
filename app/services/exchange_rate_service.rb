class ExchangeRateService

  def exchange_rate_differences(exchange_rates, serialize_for_mailer = false)
    # Increase = New Number - Original Number
    # Ako je minus, onda se radi o padu vrijednosti
    @incr = []
    exchange_rates.map! do |rate|
      difference_buying = rate[:ending].buying_rate - rate[:starting].buying_rate
      difference_middle = rate[:ending].middle_rate - rate[:starting].middle_rate
      difference_selling = rate[:ending].selling_rate - rate[:starting].selling_rate

      difference_buying = (difference_buying / rate[:ending].buying_rate) * 100
      difference_middle = (difference_middle / rate[:ending].middle_rate) * 100
      difference_selling = (difference_selling / rate[:ending].middle_rate) * 100
      if serialize_for_mailer
        rate = {
          :limit => rate[:limit] ? rate[:limit] : 0.0,
          :starting => rate[:starting].id.to_s,
          :ending => rate[:ending].id.to_s,
          :difference_buying => difference_buying.round(3),
          :difference_middle => difference_middle.round(3),
          :difference_selling => difference_selling.round(3)
        }
      else
        rate = {
          :limit => rate[:limit] ? rate[:limit] : 0.0,
          :starting => rate[:starting],
          :ending => rate[:ending],
          :difference_buying => difference_buying.round(3),
          :difference_middle => difference_middle.round(3),
          :difference_selling => difference_selling.round(3)
        }
      end
    end
    exchange_rates.delete_if{ |reject_if| reject_if[:difference_middle].abs < reject_if[:limit] }
    return exchange_rates
  end

end
