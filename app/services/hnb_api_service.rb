require 'net/http'
require 'json'
require 'active_support'
require 'active_support/core_ext'


class HnbApiService
  include ExchangeRateHelper

  def update()
    exchange_rates = get_exchange_rates() #array objekata
    exchange_rates.each do |rate|
      date = DateTime.parse(rate["Datum primjene"])
      exchange = ExchangeRate.new({
        start_date: date,
        buying_rate: ExchangeRateHelper.string_to_float(rate["Kupovni za devize"]),
        middle_rate: ExchangeRateHelper.string_to_float(rate["Srednji za devize"]),
        selling_rate: ExchangeRateHelper.string_to_float(rate["Prodajni za devize"])
      })
      exchange.currencies = Currency.where(:currency => rate["Valuta"]).first
      if exchange.currencies
        exchange.save!
      end
    end
  end

  def first_time_update()
    exchange_rates = get_exchange_rates() #array objekata
    exchange_rates.each do |rate|
      date = DateTime.parse(rate["Datum primjene"])
      currency = Currency.new({
        currency: rate["Valuta"],
        state: rate["Država"],
        currency_code: rate["Šifra valute"],
        currency_unit: rate["Jedinica"]
      })
      currency.save!
      exchange = ExchangeRate.new({
        date: date,
        buying_rate: ExchangeRateHelper.string_to_float(rate["Kupovni za devize"]),
        middle_rate: ExchangeRateHelper.string_to_float(rate["Srednji za devize"]),
        selling_rate: ExchangeRateHelper.string_to_float(rate["Prodajni za devize"])
      })
      exchange.currencies = currency
      exchange.save!
    end
  end

  def get_exchange_rates()
    uri = URI(ENV['hnb_api'])
    response = Net::HTTP.get(uri)
    response = JSON.parse(response)
    return response
  end
end
