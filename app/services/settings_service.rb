class SettingsService
  def create(set_create_params, user)
    date = DateTime.parse(set_create_params[:start_date])
    currency = Currency.where(:currency => set_create_params[:currency]).first
    ex_rate = ExchangeRate.where(currencies: currency).and(start_date: date).first
    setting = Setting.new({
      limit: set_create_params[:limit],
      start_date: set_create_params[:start_date],
      users: user,
      exchange_rate: ex_rate,
      created_at: DateTime.now
    })
    if setting.update_user
      setting.save!
      return true
    else
      return false
    end
    byebug
  end
end
