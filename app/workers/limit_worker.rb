require 'json'

class LimitWorker
  include Sidekiq::Worker
  include ExchangeRateHelper

  def perform
    ExchangeRateHelper.check_user_limits
  end
end

