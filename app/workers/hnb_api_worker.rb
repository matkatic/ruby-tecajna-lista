class HnbApiWorker
  include Sidekiq::Worker

  def perform
    HnbApiService.update()
    LimitWorker.perform_async
  end
end
