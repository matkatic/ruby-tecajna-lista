require 'json'

class MailWorker
  include Sidekiq::Worker

  def perform(tecaj_id_collection, user_id)
    TecajMailer.dnevni_tecaj(tecaj_id_collection, user_id).deliver
  end
end

