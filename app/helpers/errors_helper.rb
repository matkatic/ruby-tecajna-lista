module ErrorsHelper extend self  

  ## DATABASE RESULT 
  def user_exists(mail)
    { :user_exists => "User with #{mail} already exists" }
  end

  ## EMPTY INPUT DATA 
  def email_missing
    { :email => "Email field cannot be empty" } 
  end
  def name_missing
    { :name => "Name field cannot be empty"}
  end
  def password_missing
    { :password => "Password field cannot be empty" }
  end
  def password_confirmation_missing
    { :password_confirmation => "Please repeat your password" }
  end
  
  ## LOGIN
  def invalid_login_attempt
    { :invalid_login_attempt => "User and/or password incorrect"}
  end
  
  ## OTHER
  def password_mismatch
    { :password_confirmation => "Password mismatch" }
  end
  def bad_input
    { :bad_input => "Check your input data!" }
  end

end
