module UserHelper extend self

  def get_current
    current_user
  end

  def get_current_user_settings
    current_user.settings
  end

end