
module ExchangeRateHelper extend self

    def set_exchange_rate(request)
      exchange_rates = []
      
      start_date = DateTime.parse(request["start_date"])
      end_date = DateTime.parse(request["end_date"])
      byebug

      currencies = Currency.where(:currencies.in => request[:currencies]).all.entries
      
      start_date = ExchangeRate.where(:start_date => start_date).in(:currencies => currencies).all.entries
      end_date = ExchangeRate.where(:start_date => end_date).in(:currencies => currencies).all.entries
      
      start_date.each do |starting|
        end_date.each do |ending|
          if starting.currencies.currency == ending.currencies.currency
            exchange_rates << {:starting => starting, :ending => ending}
          end
        end
      end
      return exchange_rates
    end
    


    def check_user_limits
      exchange_rates = []
      
      users = User.all.entries
      users.each do |user| # za svakog usera
        user.user_settings.each do |setting| # settingsi se loopaju i sastavlja se exchange_rates array
          ending_rate = ExchangeRate.where(currencies: setting.exchange_rate.currencies).and(start_date: DateTime.parse(Date.today.prev_day.to_s)).first
          exchange_rates << {:starting => setting.exchange_rate, :ending => ending_rate, :limit => setting.limit}
        end
        exchange_rate_service = ExchangeRateService.new
        exchange_rates = exchange_rate_service.exchange_rate_differences(exchange_rates, true) # vraca array deviza na odabrani i danasnji datum te njihove razlike u postotku

        if exchange_rates.nil? || exchange_rates.empty?
          pp "nula bodova"
        else
          MailWorker.perform_async(exchange_rates, user.id)
        end

      end
    end

    def get_by_date(params)
      date = DateTime.parse(params[:date])
      currencies = Currency.where(:currency.in => params[:currencies].split(',')).all.entries
      exchange_rates = ExchangeRate.where(start_date: date).in(currencies: currencies).all.entries
    end

    def string_to_float(string)
      string.gsub!(/\,/,".")
      string.to_f
    end

end