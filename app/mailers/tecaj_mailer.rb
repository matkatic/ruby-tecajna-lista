class TecajMailer < ApplicationMailer
  default from: "hnbApi@code.consulting"
  # before_action :email_exists!
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.tecaj_mailer.dnevni_tecaj.subject
  #
  def dnevni_tecaj(exchange_rates, user_id)
    @end_date
    @prev_date
    @user = User.where(:id => user_id["$oid"]).pluck(:email).first
    @exchange_rates = exchange_rates
    @exchange_rates.map! do |exchange_rate|
      @end_date = ExchangeRate.where(:id => exchange_rate["ending"]).pluck(:start_date).first
      exchange_rate = {
        "starting" => ExchangeRate.where(:id => exchange_rate["starting"]).entries.first,
        "ending" => ExchangeRate.where(:id => exchange_rate["ending"]).entries.first,
        "difference_buying" => exchange_rate["difference_buying"],
        "difference_middle" => exchange_rate["difference_middle"],
        "difference_selling" => exchange_rate["difference_selling"]
      }
    end
    pp @exchange_rates
    mail to: @user
  end

end
