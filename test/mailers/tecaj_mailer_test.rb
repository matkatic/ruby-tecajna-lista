require 'test_helper'

class TecajMailerTest < ActionMailer::TestCase
  test "dnevni_tecaj" do
    mail = TecajMailer.dnevni_tecaj
    assert_equal "Dnevni tecaj", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
