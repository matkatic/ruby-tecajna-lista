# Preview all emails at http://localhost:3000/rails/mailers/tecaj_mailer
class TecajMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/tecaj_mailer/dnevni_tecaj
  def dnevni_tecaj
    TecajMailer.dnevni_tecaj
  end

end
