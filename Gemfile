source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end

# MongoDB support
gem 'mongoid', git: 'https://github.com/mongodb/mongoid.git'
# Mongoid_alize gem for easy model relationships
# gem 'mongoid_alize', git: 'https://github.com/dzello/mongoid_alize'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.6'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Simple, efficient background processing for Ruby.
gem 'sidekiq'
gem 'sidekiq-cron', git: "https://github.com/ondrejbartas/sidekiq-cron"
gem 'redis-namespace'

# Devise is a flexible authentication solution for Rails based on Warden. It:
gem 'devise', git: 'https://github.com/plataformatec/devise'
# For Json Serialization
gem 'active_model_serializers', '~> 0.10.2'
# Use rack/cors for handling Cross-Origin Resource Sharing
# More info: https://github.com/cyu/rack-cors
gem 'rack-cors', require: 'rack/cors'

# Whenever gem for Cron Jobs
# More info: https://github.com/javan/whenever
gem 'whenever', require: false

# Easy require statents 
# More info: https://github.com/jarmo/require_all
gem 'require_all'

# Figaro enviromental variables
gem "figaro", git:'https://github.com/laserlemon/figaro'


# Debugger https://github.com/rubyide/vscode-ruby/wiki/Debugger

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
