Rails.application.routes.draw do


  # API ONLY ROUTES #
  scope :api do
    
    # DEVISE AUTHENTICATION AND REGISTRATION #
    devise_for :users, class_name: 'User'#,  controllers: { sessions: "users/sessions", registration: "user/registrations" }
    devise_scope :user do
      post 'login', to: 'users/sessions#create'
      post 'register', to: 'users/registrations#create'
    end

    # APPLICATION RESOURCES #
    resources :settings
    resources :exchange_rates
    resources :currencies
    # resource :login, only: [:create], controller: "users/sessions"

    # ADDITIONAL ROUTES #
    get 'test/' => 'currencies#hnbApi'
    get 'first/' => 'currencies#first'

    get 'exchange-rate/difference' => 'exchange_rates#exchange_rate_difference'
    get 'exchange-rate/get-by-date' => 'exchange_rates#get_by_date'

  end

    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
